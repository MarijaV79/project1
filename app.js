var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var loginRouter = require('./routes/login');
var signupRouter = require('./routes/signup');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/', loginRouter.router);
app.use(signupRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



//1
// Access the parse results as request.body
/*app.post('/', function(request, response){
  console.log("POST");
  console.log(request.body.username);
  console.log(request.body.password);
});*/



//2
/*app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.get("/", function(req, res) { 
  res.sendFile(__dirname + "/index.html"); 
}); 
  

app.post('/', function(req, res) {
  console.log("POST");
  var post_body = req.body;
  var username = req.body.uname;
  console.log("Username:", username);

  console.log(post_body);

  // Return the POST message
  res.send(post_body);
});
*/

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});



module.exports = app;
