require('dotenv').config({path: `/home/marija/project1/process.env`});
var express = require('express');
var bcrypt = require('bcryptjs');
var router = express.Router();

const pool = require('/home/marija/project1/routes/db_connection.js');


router.post('/signup', function(req, res) {
  var post_body = req.body;
  var email = req.body.email;
  var username = req.body.uname;
  var password = req.body.psw;
  var confirmPassword = req.body.pswRepeat;

  console.log("Email:", email);
  console.log("Username:", username);
  console.log("Password:", password);
  console.log("Confirm password:", confirmPassword);

  if(password==confirmPassword){
    var hashPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
    console.log(hashPassword);
    console.log(post_body);
    console.log("");

    var avatar = "profile_photo.png";

    pool.query(
      'INSERT INTO "user_data" (username, email, password, avatar) VALUES ($1, $2, $3, $4)', 
      [username, email, hashPassword, avatar],
      (err, res2) => {
        console.log(err, res2);

        if(err==undefined){
          console.log("Registration was successful.");
          res.redirect("/signup/status");
        }else{
          console.log("ERROR! Username or password or email already exists.");
          res.status(500).redirect("/signup");
        }
      }
    );

  }else{
    console.log("Password does not match.");
    res.redirect("/signup");
  }
});
  
  
  
module.exports = router;