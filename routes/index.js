//require('dotenv').config({path: `${__dirname}/process.env`});
//require('dotenv').config({path: __dirname + '/process.env'})
require('dotenv').config({path: `/home/marija/project1/process.env`});
var express = require('express');
var bcrypt = require('bcryptjs');
var crypto = require('crypto-js'); 
const multer = require('multer');
const path = require('path');
var router = express.Router();

const pool = require('/home/marija/project1/routes/db_connection.js');

var login = require('/home/marija/project1/routes/login.js');
const authTokens = login.authTokens;



/*session = require('express-session');
router.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));*/



const storage = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, 'public/images');
  },
  filename: (req, file, cb) => {
      console.log("file");
      console.log(file);
      console.log(file.originalname);
      console.log("");
      //cb(null, Date.now() + path.extname(file.originalname));
      cb(null, file.originalname);
  }
});
const fileFilter = (req, file, cb) => {
  if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
      cb(null, true);
  } else {
      console.log("File is not jpg/png.");
      cb(null, false);
      //cb(new Error('I don\'t have a clue!'));
  }
}
const upload = multer({ storage: storage, fileFilter: fileFilter });



//Upload route
router.post('/upload', upload.single('image'), (req, res, next) => {
  try {
      console.log("req body");
      console.log(req.body);

      return res.status(201).json({
          message: 'File uploaded successfully'
      });
  } catch (error) {
      console.error(error);
  }
});


router.post('/profile', upload.single('image'), (req, res, next) => {
  try {
      console.log("REQ BODY PROFILE");
      console.log(req.body);

      var avatar = req.file.originalname;
      console.log(avatar);

      const authToken = req.cookies['AuthToken'];
      var hex = crypto.enc.Hex.stringify(authToken);
      var username = authTokens[hex];
      console.log(username);

      
      pool.query( 
        'UPDATE user_data SET avatar=($1) WHERE username=($2)',
        [avatar, username],
        (err, res2) => {
          //console.log(err, res2);
          console.log("PROFILE");
  
          if(err==undefined){
            console.log("No errors.");
            res.status(201).redirect('/profile');
  
          }else{
            console.log("ERROR!");
            res.status(201).redirect('/profile');
          }
      
        }
      );

      /*return res.status(201).json({
          message: 'File uploaded successfully'
      });*/
  } catch (error) {
      console.error(error);
  }
});


router.get('/upload', function(req, res, next) {
  res.render('upload');
});



/* GET home page. */
router.get('/', function(req, res, next) {
  //console.log(require('dotenv').config())
  
  res.render('index', { errormessage: 'Please login to continue.' });
});


router.get('/signup', function(req, res, next) {
  res.render('signup');
});


router.get('/signup/status', function(req, res, next) {
  res.render('reg_status');
});


router.get('/logout', function(req, res, next) {
  console.log("Logout");
  const authToken = req.cookies['AuthToken'];
  var hex = crypto.enc.Hex.stringify(authToken);
  console.log(hex);

  delete authTokens[hex];
  console.log(authTokens);

  res.status(200).render('logout');
});



router.get('/homepage', function(req, res, next) {
  const authToken = req.cookies['AuthToken'];
  console.log("HOMEPAGE");
  console.log(authTokens);
  
  var hex = crypto.enc.Hex.stringify(authToken);
  var username = authTokens[hex];

  console.log(hex);
  console.log("User:", username);
  

  if(username!=undefined){

    pool.query(
      'SELECT user_posts.username, post_text, post_image, timestamp, post_id, avatar FROM "user_posts" LEFT JOIN "user_data" ON user_posts.username=user_data.username ORDER BY "timestamp" DESC',
      (err, res2) => {
        //console.log(err, res2);
        
        if(err==undefined){
          console.log("No errors.");
          var user_posts = res2.rows;
          console.log("DATA");
          console.log(user_posts);


          pool.query(
            'SELECT post_id, user_comments.username, user_comment, timestamp, avatar FROM "user_comments" LEFT JOIN "user_data" ON user_comments.username=user_data.username ORDER BY "timestamp"',
            (err, res3) => {
              //console.log(err, res3);
              
              if(err==undefined){
                console.log("No errors.");
                var user_comments = res3.rows;
                console.log("DATA2");
                console.log(user_comments);

                /*for(var i=0; i < data2.length; i++){
                  var local = data2[i]["timestamp"].toLocaleString();
                  data2[i]["timestamp"] = local;
                }*/

                var comments_num = {};
                for(var i=0; i < user_posts.length; i++){
                  var br=0;
                  for(var j=0; j < user_comments.length; j++){
                    if(user_posts[i]["post_id"] == user_comments[j]["post_id"]){
                      br++;
                    }
                  }
                  comments_num[user_posts[i]["post_id"]] = br;
                }


                var notifications = [];
                for(var i=0; i < user_posts.length; i++){
                  if(user_posts[i]["username"] == username){

                    for(var j=0; j < user_comments.length; j++){
                      var notification = {};
                      if(user_posts[i]["post_id"] == user_comments[j]["post_id"] && user_comments[j]["username"] != username){
                        notification["username"] = user_comments[j]["username"];
                        notification["user_comment"] = user_comments[j]["user_comment"];
                        notification["timestamp"] = user_comments[j]["timestamp"];
                        notification["post_id"] = user_comments[j]["post_id"];
                        notification["avatar"] = user_comments[j]["avatar"];

                        notifications.push(notification);
                      }
                    }
                  }
                }

                console.log();
                console.log("NOTIFICATIONS");
                console.log(notifications);

                notifications = notifications.sort(function(a,b){
                  return new Date(b.timestamp) - new Date(a.timestamp);
                });

                console.log("NOTIFICATIONS2");
                console.log(notifications);

                for(var i=0; i < notifications.length; i++){
                  notifications[i].timestamp = notifications[i].timestamp.toLocaleString();
                }
      
                res.status(200).render('homepage', {user_posts: user_posts, username: username, user_comments: user_comments, 
                  comments_num: comments_num, notifications: notifications});
      
              }else{
                console.log("ERROR!");
                console.log(err);
      
                res.status(500).render('homepage');
              }
          
            }
          );

        }else{
          console.log("ERROR!");
          console.log(err);

          res.status(500).render('homepage');
        }
      }
    );

  }else{
    console.log("UNDEFINED");
    res.redirect('/');
  }

});



router.post('/homepage', function(req, res) {
  console.log("HOMEPAGE POST");
  const authToken = req.cookies['AuthToken'];
  var hex = crypto.enc.Hex.stringify(authToken);
  console.log(hex);
  console.log(authTokens);

  var username = authTokens[hex];
  var post_text = req.body.user_post;
  var post_image = "";

  console.log(req.body);
  console.log(username);
  console.log(post_text);

  var time = new Date();
  console.log("Time:")
  console.log(time);

  var dateString = time.toDateString();
  console.log("toDateString:\n", dateString);

  var timeString = time.toString();
  console.log("toString:\n", timeString);

  var ISOstring = time.toISOString();
  console.log("toISOstring:\n", ISOstring);

  var localeString = time.toLocaleString();
  console.log("toLocaleString:\n", localeString);

  //var cut = time.toString().replace(/T/, ' ');
  //console.log(cut);

  var time2 = time.toISOString().replace(/\.\d+/, "");
  console.log("Time2:", time2);

  console.log("");

  pool.query(
    'INSERT INTO "user_posts" (username, post_text, post_image, timestamp) VALUES ($1, $2, $3, $4)', 
    [username, post_text, post_image, time2],
    (err, res2) => {
      console.log(err, res2);

      if(err==undefined){
        console.log("Success.");
        res.redirect("/homepage");
      }else{
        console.log("ERROR!");
        res.redirect('/homepage');
      }
    }
  );
  
});



router.post('/homepage_comment', function(req, res) {
  console.log("HOMEPAGE POST COMMENT");
  const authToken = req.cookies['AuthToken'];
  var hex = crypto.enc.Hex.stringify(authToken);
  console.log(hex);
  console.log(authTokens);

  var username = authTokens[hex];

  console.log(username);
  console.log(req.body);

  var user_comment = req.body.user_comment;
  var post_id = req.body.post_id;

  var time = new Date();
  console.log("Time:", time);

  var time2 = time.toISOString().replace(/\.\d+/, "");
  console.log("Time2:", time2);


  console.log("");

  pool.query(
    'INSERT INTO "user_comments" (post_id, username, user_comment, timestamp) VALUES ($1, $2, $3, $4)', 
    [post_id, username, user_comment, time2],
    (err, res2) => {
      console.log(err, res2);

      if(err==undefined){
        console.log("Success.");
        res.redirect("/homepage");
      }else{
        console.log("ERROR!");
        res.redirect('/homepage');
      }
    }
  );
});



router.get('/profile', function(req, res, next) {
  const authToken = req.cookies['AuthToken'];
  var hex = crypto.enc.Hex.stringify(authToken);
  var username = authTokens[hex];
  var email = "";
  var avatar = "";

  console.log("PROFILE!");
  console.log(hex);
  console.log("User:", username);


  if(username!=undefined){

    pool.query(
      'SELECT user_posts.username, post_text, post_image, timestamp, post_id, email, avatar FROM "user_posts" LEFT JOIN "user_data" ON user_posts.username=user_data.username WHERE user_data.username=($1) ORDER BY "timestamp" DESC',
      [username],
      (err, res2) => {
        //console.log(err, res2);
        
        if(err==undefined){
          console.log("No errors.");
          var user_posts = res2.rows;
          console.log("DATA");
          console.log(user_posts);


          if(user_posts.length==0){
            pool.query(
              'SELECT username, email, avatar FROM "user_data" WHERE username=($1)', 
              [username],
              (err, res3) => {
                //console.log(err, res3);
          
                if(err==undefined){
                  console.log("Success.");
                  var data2 = res3.rows;
                  console.log("DATA2");
                  console.log(data2);

                  avatar = res3.rows[0].avatar;
                  email = res3.rows[0].email;
                  var notifications = [];

                  res.status(200).render('profile', {user_posts: user_posts, username: username, avatar: avatar, email: email,
                  notifications: notifications});

                }else{
                  console.log("ERROR!");
                  res.status(500).render('profile');
                }
              }
            );

          }else{
            pool.query(
              'SELECT post_id, user_comments.username, user_comment, timestamp, avatar FROM "user_comments" LEFT JOIN "user_data" ON user_comments.username=user_data.username ORDER BY "timestamp"',
              (err, res3) => {
                //console.log(err, res3);
                
                if(err==undefined){
                  console.log("No errors.");
                  var user_comments = res3.rows;
                  console.log("DATA2");
                  console.log(user_comments);

                  avatar = res2.rows[0].avatar;
                  email = res2.rows[0].email;
  
                  /*for(var i=0; i < user_comments.length; i++){
                    var local = user_comments[i]["timestamp"].toLocaleString();
                    user_comments[i]["timestamp"] = local;
                  }*/
  
                  var comments_num = {};
                  for(var i=0; i < user_posts.length; i++){
                    var br=0;
                    for(var j=0; j < user_comments.length; j++){
                      if(user_posts[i]["post_id"] == user_comments[j]["post_id"]){
                        br++;
                      }
                    }
                    comments_num[user_posts[i]["post_id"]] = br;
                  }


                  var notifications = [];
                  for(var i=0; i < user_posts.length; i++){
                    for(var j=0; j < user_comments.length; j++){
                      var notification = {};
                      if(user_posts[i]["post_id"] == user_comments[j]["post_id"] && user_comments[j]["username"] != username){
                        notification["username"] = user_comments[j]["username"];
                        notification["user_comment"] = user_comments[j]["user_comment"];
                        notification["timestamp"] = user_comments[j]["timestamp"];
                        notification["post_id"] = user_comments[j]["post_id"];
                        notification["avatar"] = user_comments[j]["avatar"];

                        notifications.push(notification);
                      }
                    }
                  }

                  console.log();
                  console.log("NOTIFICATIONS");
                  console.log(notifications);

                  notifications = notifications.sort(function(a,b){
                    return new Date(b.timestamp) - new Date(a.timestamp);
                  });
  
                  console.log("NOTIFICATIONS2");
                  console.log(notifications);
  
                  for(var i=0; i < notifications.length; i++){
                    notifications[i].timestamp = notifications[i].timestamp.toLocaleString();
                  }

                  res.status(200).render('profile', {user_posts: user_posts, username: username, avatar: avatar, email: email, 
                    user_comments: user_comments, comments_num: comments_num, notifications: notifications});
        
                }else{
                  console.log("ERROR!");
                  console.log(err);
        
                  res.status(500).render('profile');
                }
            
              }
            );
          }

        }else{
          console.log("ERROR!");
          console.log(err);

          res.status(500).render('profile');
        }
      }
    );

  }else{
    console.log("UNDEFINED");
    res.redirect('/');
  }
});


router.get('/profile/:username', function(req, res, next) {
  const authToken = req.cookies['AuthToken'];
  var hex = crypto.enc.Hex.stringify(authToken);
  var user = authTokens[hex];
  var username = req.originalUrl.split("/")[2];
  var avatar;

  console.log("User:", user);
  console.log("Username:", username);


  if(user!=undefined){

    if(user==username){
      res.redirect('/profile');
    }

    pool.query(
      'SELECT user_data.username, avatar, post_text, post_image, timestamp, post_id FROM "user_data" LEFT JOIN "user_posts" ON user_data.username=user_posts.username WHERE user_data.username=($1) ORDER BY "timestamp" DESC',
      [username],
      (err, res2) => {
        //console.log(err, res2);
        
        if(err==undefined){
          console.log("No errors.");

          avatar = res2.rows[0].avatar;
          var user_posts = res2.rows;
          console.log(user_posts);

          var post_id = res2.rows[0].post_id;

          if(post_id==null){
            user_posts = {};

            res.status(200).render('user_profile', {user_posts: user_posts, username: username, avatar: avatar});

          }else{
            pool.query(
              'SELECT post_id, user_comments.username, user_comment, timestamp, avatar FROM "user_comments" LEFT JOIN "user_data" ON user_comments.username=user_data.username ORDER BY "timestamp"',
              (err, res3) => {
                //console.log(err, res3);
                
                if(err==undefined){
                  console.log("No errors.");
                  var user_comments = res3.rows;
                  console.log("DATA2");
                  console.log(user_comments);

                  //avatar = res2.rows[0].avatar;

                  /*for(var i=0; i < user_comments.length; i++){
                    var local = user_comments[i]["timestamp"].toLocaleString();
                    user_comments[i]["timestamp"] = local;
                  }*/

                  var comments_num = {};
                  for(var i=0; i < user_posts.length; i++){
                    var br=0;
                    for(var j=0; j < user_comments.length; j++){
                      if(user_posts[i]["post_id"] == user_comments[j]["post_id"]){
                        br++;
                      }
                    }
                    comments_num[user_posts[i]["post_id"]] = br;
                  }

                  res.status(200).render('user_profile', {user: user, user_posts: user_posts, username: username, avatar: avatar, 
                    user_comments: user_comments, comments_num: comments_num });
        
                }else{
                  console.log("ERROR!");
                  console.log(err);
        
                  res.status(500).render('user_profile');
                }
            
              }
            );

          }

        }else{
          console.log("ERROR!");
          console.log(err);

          res.status(500).render('user_profile', { username: username, avatar: avatar });
        }
    
      }
    );

  }else{
    console.log("UNDEFINED");
    res.redirect('/');
  }
});


router.post('/profile_post', function(req, res) {
  console.log("HOMEPAGE POST");
  const authToken = req.cookies['AuthToken'];
  var hex = crypto.enc.Hex.stringify(authToken);
  console.log(hex);
  console.log(authTokens);

  var username = authTokens[hex];
  var post_text = req.body.user_post;
  var post_image = "";

  console.log(req.body);
  console.log(username);

  var time = new Date();
  console.log("Time:")
  console.log(time);

  var time2 = time.toISOString().replace(/\.\d+/, "");
  console.log("Time2:", time2);


  console.log("");

  pool.query(
    'INSERT INTO "user_posts" (username, post_text, post_image, timestamp) VALUES ($1, $2, $3, $4)', 
    [username, post_text, post_image, time2],
    (err, res2) => {
      console.log(err, res2);

      if(err==undefined){
        console.log("Success.");
        res.status(200).redirect("/profile");
      }else{
        console.log("ERROR!");
        res.status(500).redirect('/profile');
      }
    }
  );
  
});



router.post('/delete_post', function(req, res) {
  console.log("");
  console.log("HOMEPAGE DELETE");
  const authToken = req.cookies['AuthToken'];
  var hex = crypto.enc.Hex.stringify(authToken);
  console.log(hex);
  console.log(authTokens);

  var username = authTokens[hex];
  console.log(username);
  console.log(req.body);
  console.log("Name:", req.body.name);

  var post_text = req.body.post_text;
  var timestamp = req.body.timestamp;
  console.log("Timestamp:", timestamp);

  console.log("-----------------------");

  var timestamp2 = new Date(timestamp).toISOString();
  console.log("NEW TIME:", timestamp2);

  var post_id = req.body.post_id;
  console.log("Post_id:", post_id);

  
  pool.query(
    'DELETE FROM "user_posts" WHERE username=($1) AND post_text=($2) AND timestamp=($3)',
    [username, post_text, timestamp2], 
    (err, res2) => {
      console.log(err, res2);

      if(err==undefined){
        console.log("Success.");

        pool.query(
          'DELETE FROM "user_comments" WHERE post_id=($1)', 
          [post_id],
          (err2, res3) => {
            console.log(err2, res3);
      
            if(err2==undefined){
              console.log("Success.");
              res.redirect('/homepage');
            }else{
              console.log("ERROR!");
              res.redirect('/homepage');
            }
          }
        );

      }else{
        console.log("ERROR!");
        res.redirect('/homepage');
      }
    }
  );
});



router.post('/comment_delete', function(req, res) {
  console.log("");
  console.log("HOMEPAGE COMMENT DELETE");
  const authToken = req.cookies['AuthToken'];
  var hex = crypto.enc.Hex.stringify(authToken);
  console.log(hex);
  console.log(authTokens);

  var username = authTokens[hex];
  console.log("Username:", username);
  console.log("Req.body:", req.body);

  var user_comment = req.body.user_comment;
  var timestamp = req.body.timestamp;
  var post_id = req.body.post_id;

  var timestamp2 = new Date(timestamp).toISOString();
  console.log("NEW TIME:", timestamp2);

  
  console.log("-----------------------");
  
  pool.query(
    'DELETE FROM "user_comments" WHERE username=($1) AND user_comment=($2) AND timestamp=($3) AND post_id=($4)',
    [username, user_comment, timestamp2, post_id], 
    (err, res2) => {
      console.log(err, res2);

      if(err==undefined){
        console.log("Success.");
        res.redirect('/homepage');

      }else{
        console.log("ERROR!");
        res.redirect('/homepage');
      }
    }
  );
});



module.exports = router;
