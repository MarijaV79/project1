//require('dotenv').config({path: `${__dirname}/process.env`});
//require('dotenv').config({path: __dirname + '/process.env'})
require('dotenv').config({path: `/home/marija/project1/process.env`});
var express = require('express');
var bcrypt = require('bcryptjs');
var crypto = require('crypto-js'); 
var router = express.Router();

const pool = require('/home/marija/project1/routes/db_connection.js');


const generateAuthToken = () => {
  //return crypto.randomBytes(30).toString('hex');
  return crypto.lib.WordArray.random(32);
  //return crypto.lib.WordArray.random(32).toString(crypto.enc.Utf8);
}

const authTokens = {};
  
  
router.post('/', function(req, res) {
  var username = req.body.uname;
  var password = req.body.psw;
  //const { username, password } = req.body;

  var user = "";
  console.log(username);
  console.log(password);
  console.log("");


  pool.query('SELECT * FROM user_data', (err, res2) => {
    if (err) {
        console.error(err);
        return;
    }

    var user_data = res2.rows;
    var iterations = user_data.length;

    for (let row of user_data) {
      var pwd = row.password;

      if(username==row.username){
        bcrypt.compare(password, pwd, function (err, result) {
          if (result == true) {
            console.log("Correct password.");
            console.log(password);
            user=username;
            console.log("User:", user);

            const authToken = generateAuthToken();
            authTokens[authToken] = user;
            res.cookie('AuthToken', authToken);

            console.log("");
            console.log(authTokens);

            res.status(200).redirect('/homepage');

          } else {
            console.log("Incorrect password.");

            if (!--iterations){
              res.status(401).render('index', { errormessage: 'Wrong username and/or password.' });
            }
          }
        });

      }else{
        if (!--iterations){
          res.status(401).render('index', { errormessage: 'Wrong username and/or password.' });
        }
      }
    }
  });
});  
    

module.exports = { 
  router:router,
  authTokens:authTokens
}