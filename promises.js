function one() {
    return new Promise(resolve => {
      console.log("one");
      resolve();
    });
  }
  
  function two() {
    return new Promise(resolve => {
      console.log("two");
      resolve();
    });
  }
  
  function three(){
     console.log("three")
  }
  
  //one().then(() => two()).then(() => three());



  async function run() {
    await one();
    await two();
    three();
  }
  run();


/*
  const makeFresh = function() {
    const isKeepingPoolOpen = true;
    dropTables(isKeepingPoolOpen)
    .then(() => createTables(isKeepingPoolOpen))
    .then(() => seedTables(isKeepingPoolOpen))
    .then(() => {
        pool.end();
    })
    .catch((err) => {
        console.log("error: " + error);
        pool.end();
    });
  }
*/